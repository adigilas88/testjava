package com.company.model;

public class Mahasiswa {
    private int id;
    private String fullname;
    private String address;
    private String status;
    private int gradesphysics;
    private int gradescalculus;
    private int gradesbiologi;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public int getGradesphysics() {
        return gradesphysics;
    }
    public void setGradesphysics(int gradesphysics) {
        this.gradesphysics = gradesphysics;
    }

    public int getGradescalculus() {
        return gradescalculus;
    }
    public void setGradescalculus(int gradescalculus) {
        this.gradescalculus = gradescalculus;
    }

    public int getGradesbiologi() {
        return gradesbiologi;
    }

    public void setGradesbiologi(int gradesbiologi) {
        this.gradesbiologi = gradesbiologi;
    }
}
